# This project provisions immudb using following tools and technologies
 | Type           | Technology            | Comments                                                                                                                                |
|----------------|-----------------------|-----------------------------------------------------------------------------------------------------------------------------------------|
 | Virtualization | Oracle VM and Vagrant | Vagrant tool to manage VM                                                                                                               |
 | Kubernetes     | Standard              | deployed using kubeadm                                                                                                                  |

# Prerequisites
* Install Oracle VM & vagrant using yum or apt depending on the linux variants
* Update the Vagrantfile with IP addresses of the contol plane and worker nodes
```
    vi vagrant/Vagrantfile
```
# Procedure for provisioning VMs and k8s cluster
```
vagrant up
```
* This deploys a 3 node cluster (1 master and 2 workers nodes)
* The bootstrap scripts install necessary packages and creates an empty k8s cluster

# Bootstrap the cluster with infra services like cert-manager, longhorn etc
```
1. scp the kubeconfif from the master node
2. git clone git@gitlab.com:amulraj/immudb.git
3. Install the infra services using helm and apply the additional manifests avaialbe under the corresponding folders

```
# Deploy immudb cluster with replica
```
kubectl apply -f k8s-manifests/db

```
# Access the DB and grafana dashboards
```
kubectl get ing -A # get the DNS and access them via browser

```
